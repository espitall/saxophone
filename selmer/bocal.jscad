function getParameterDefinitions() {
  return [
	{ name: 'slices', caption: 'Slices:', type: 'int', default: 10 },
	{ name: 'c_polygon', caption: 'Circle polygons:', type: 'int', default: 10 },
	{ name: 'angle', caption: 'Angle :', type: 'float', default: 12.5 },
	{ name: 'c_radius', caption: 'Radius:', type: 'float', default: 55},
	{ name: 'linear_length', caption: 'Linear length:', type: 'float', default: 105 },
	{ name: 'small_external_diameter', caption: 'Small diameter:', type: 'float', default: 13.5 },
	{ name: 'large_external_diameter', caption: 'Large diameter:', type: 'float', default: 24.5 },
	{ name: 'in_body_lentgh', caption: 'In body:', type: 'float', default: 21.5},
	{ name: 'screw', caption: 'Screw:', type: 'float', default: 50},
	{ name: 'parts', type: 'slider', min: 0, max: 3, caption: 'Display', default: 0},
  ];
}

function createPolygonCircle(params, radius, y_radius) {
    
    if(y_radius === undefined) {
        y_radius = radius;
    }
    
    const array_pts = [];
    for(let i = 0; i < params.c_polygon; i += 1)
    {
        array_pts.push([
            radius * Math.cos(i / params.c_polygon * 2 * Math.PI),
            y_radius * Math.sin(i / params.c_polygon * 2 * Math.PI),
            0
        ]);
    }
    
    return CSG.Polygon.createFromPoints(array_pts);
}

function getSlice(params, external, step) {
    let start_in_body_sax = 0.0;
    let end_in_body_sax = 2.0 / (params.slices - 1);
    let start_fix_bed = 1.0 - 2.0 / (params.slices - 1);
    let end_fix_bed = 1.0;
    let start_linear = start_fix_bed - 2.0 / (params.slices - 1);
    let end_linear = start_fix_bed;
    let start_circle = end_in_body_sax;
    let end_circle = start_linear;
    
    const thickness = 0.8;
    const fix_bed = 35;
    const fix_bed_radius = 15.5 / 2;
    
    if(params.parts == 1) {
        end_circle = 2 - start_circle;
    }
    else if(params.parts == 2) { 
        end_circle = 1;
        start_circle = 0 - end_circle;
        end_in_body_sax = start_circle - 1;
    }
    else if(params.parts == 3) {
        start_linear = 0;
        end_circle = -1;
        end_in_body_sax = start_circle - 1;
    }
    console.log([start_circle, end_circle, step]);
    
    const getStepValue = (step, start, end) => {
        return (step - start) / (end - start);
    };
    
    if((step >= start_in_body_sax) &&(step <= end_in_body_sax)) {
        //start of base in saxophone body
        const z_coord = -params.in_body_lentgh * (1.0 - getStepValue(step, start_in_body_sax, end_in_body_sax));
        
        let radius = params.large_external_diameter / 2;
        if(!external) {
            radius -= thickness;
        }
        
        return createPolygonCircle(params, radius)
            .translate([0, 0, z_coord])
            .setColor(css2rgb('red'));
    }
    else if((step >= start_circle) &&(step <= end_circle)) {
        //start curved shape
        const t = getStepValue(step, start_circle, end_circle);
        const theta = (90 - params.angle) * t;
        const x = params.c_radius * (Math.cos(theta * Math.PI / 180.0) - 1);
        const z = params.c_radius * Math.sin(theta * Math.PI / 180.0);
        
        const total_l = params.c_radius * (90 - params.angle) * Math.PI / 180.0 + params.linear_length;
        const delta_l = params.c_radius * theta * Math.PI / 180.0;
        let radius = (params.large_external_diameter + (params.small_external_diameter - params.large_external_diameter) * delta_l / total_l) / 2;
        let y_radius = radius;
        if(!external) {
            radius -= thickness;
            y_radius -= thickness;
        }
        else {
            radius += thickness;
            if(t < 0.5) {
                y_radius = (radius * 2 + (params.screw - radius * 2) * t * 2) / 2;    
            }
            else {
                y_radius = params.screw / 2;    
            }
        }
      
        return createPolygonCircle(params, radius, y_radius)
            .rotateY(-theta)
            .translate([x, 0, z])
            .setColor(css2rgb('orange'));
    }
    else if((step >= start_linear) &&(step <= end_linear)) {
        //start linear shape
        const x_start = params.c_radius * (Math.cos((90 - params.angle) * Math.PI / 180.0) - 1);
        const z_start = params.c_radius * Math.sin((90 - params.angle) * Math.PI / 180.0);
        const x_end = x_start - (params.linear_length - fix_bed) * Math.cos(params.angle * Math.PI / 180.0); 
        const z_end = z_start + (params.linear_length - fix_bed) * Math.sin(params.angle * Math.PI / 180.0);
        const t = getStepValue(step, start_linear, end_linear);
        
        const x_coord = x_start + (x_end - x_start) * t;
        const z_coord = z_start + (z_end - z_start) * t;
        
        const total_l = params.c_radius * (90 - params.angle) * Math.PI / 180.0 + params.linear_length;
        const delta_l = total_l - (1.0 - t) * params.linear_length;
        let radius = (params.large_external_diameter + (params.small_external_diameter - params.large_external_diameter) * delta_l / total_l) / 2;
        let y_radius = radius;
        if(!external) {
            radius -= thickness;
            y_radius -= thickness;
        }
        else {
            radius += thickness;
            y_radius = (radius - (params.screw / 2)) * t + params.screw / 2;
        }
        

        return createPolygonCircle(params, radius, y_radius)
            .rotateY(-(90 - params.angle))
            .translate([x_coord, 0, z_coord])
            .setColor(css2rgb('green'));
    }
    else if((step >= start_fix_bed) &&(step <= end_fix_bed)) {
        //start linear shape
        const x_start = params.c_radius * (Math.cos((90 - params.angle) * Math.PI / 180.0) - 1);
        const z_start = params.c_radius * Math.sin((90 - params.angle) * Math.PI / 180.0);
        const x_start2 = x_start - (params.linear_length - fix_bed) * Math.cos(params.angle * Math.PI / 180.0); 
        const z_start2 = z_start + (params.linear_length - fix_bed) * Math.sin(params.angle * Math.PI / 180.0);
        const x_end = x_start2 - fix_bed * Math.cos(params.angle * Math.PI / 180.0); 
        const z_end = z_start2 + fix_bed * Math.sin(params.angle * Math.PI / 180.0);
        const t = getStepValue(step, start_fix_bed, end_fix_bed);
        
        const x_coord = x_start2 + (x_end - x_start2) * t;
        const z_coord = z_start2 + (z_end - z_start2) * t;
        
        const total_l = params.c_radius * (90 - params.angle) * Math.PI / 180.0 + params.linear_length;
        const delta_l = total_l - (1.0 - t) * fix_bed;
        let radius = (params.large_external_diameter + (params.small_external_diameter - params.large_external_diameter) * delta_l / total_l) / 2;
        if(!external) {
            radius -= thickness;
        }
        else {
            radius = fix_bed_radius;
        }
        

        return createPolygonCircle(params, radius)
            .rotateY(-(90 - params.angle))
            .translate([x_coord, 0, z_coord])
            .setColor(css2rgb('yellow'));
    }
    
    console.log("No slice ?");
}


function neckShape(params, external) {
    return getSlice(params, true, 0).solidFromSlices({
        numslices: params.slices
        ,callback: function(t) {
    		return getSlice(params, external, t);
    	}
    });
}

function csgFromSegments (segments) {
  let output = [];
  segments.forEach(segment => output.push(
    rectangular_extrude(segment, { w:5, h:10 })
  ));

  return union(output);
}


function getM3nut(height) {
    const array_pts = [];
    for(let i = 0; i < 6; i += 1)
    {
        array_pts.push([
            6.75 * Math.cos(i / 6 * 2 * Math.PI) / 2,
            6.75 * Math.sin(i / 6 * 2 * Math.PI) / 2,
            0
        ]);
    }
    
    return linear_extrude({height: height }, polygon(array_pts));
}

function getScrew(params, inverse, ratio_angle) {
    return cylinder({
        r: 3.5/2, 
        h: 200,
    })
    .translate([0, 0, -100])
    .union(
        cylinder({
            r: 7 / 2,
            h: 200,
        }).translate([0, 0, 5])
    )
    .union(
        getM3nut(100)
        .translate([0, 0, -100 - 5])
    )
    .rotateX(inverse ? 180 : 0)
    .translate([params.c_radius, 0, 0])
    .rotateY((-90 + params.angle) * ratio_angle)
    .translate([-params.c_radius, 0, 0])

    .setColor(css2rgb('blue'));
}


function main(params) {
    const external_shape = neckShape(params, true);
    const internal_shape = neckShape(params, false);
    const neck_stop = cylinder({
        r: params.large_external_diameter / 2 + 1.5, 
        h: 6.5
    }).setColor(css2rgb('lightblue'));
    const screw_1 = getScrew(params, false, 0.5);
    const screw_2 = getScrew(params, true, 1);
    
    const txt_params = params.angle + '/' + params.c_radius + '/' + params.linear_length;
    const txt = csgFromSegments(vectorText({input: txt_params }))
        .rotateX(-90)
        .rotateY(-90)
        .scale(0.25)
        .translate([-5, -1, -params.in_body_lentgh + 2])
        .setColor(css2rgb("gray"));
    
    const cut = cube([1000, 1000, 1000]).translate([-500, 0, -500]);
 
    let part = external_shape;
    
     if((params.parts == 1) || (params.parts == 0)) {
          part = part
             .union(neck_stop);
     }
     
    part = part
    .subtract(screw_1.translate([0, -params.screw / 2 + 7, 0]))
    .subtract(screw_1.translate([0, params.screw / 2 - 7, 0]))
    .subtract(screw_2.translate([0, -params.screw / 2 + 7, 0]))
    .subtract(screw_2.translate([0, params.screw / 2 - 7, 0]))
    .subtract(internal_shape)
    //.subtract(cut)
    //.subtract(cut.translate([0, -1000-0.3, 0]))
    //.subtract(txt);
    
     if((params.parts == 1) || (params.parts == 0)) {
        //  part = part
        //     .union(neck_stop);
     }
    
/*
    if(!params.p1) {
        const two_parts_cut = cube([500, 500, 500])
         .translate([0, -250, -500])
          .rotateY(-(90 - params.angle) / 2)
         .translate([-params.c_radius, 0, 0]);
         
        part = part.subtract(two_parts_cut);
    }
    
    if(!params.p2) {
        const two_parts_cut = cube([500, 500, 500])
         .translate([-250, -250, 0])
          .rotateY(-(90 - params.angle) / 2)
         .translate([-params.c_radius, 0, 0]);
         
        part = part.subtract(two_parts_cut);
    }
    */
    
    return part;
}
